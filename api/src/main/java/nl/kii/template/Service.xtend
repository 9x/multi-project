package nl.kii.template

import io.vertx.codegen.annotations.ProxyGen
import io.vertx.core.AsyncResult
import io.vertx.core.Handler

@ProxyGen
interface Service {
    
    def void healthCheck( Handler<AsyncResult<Void>> result )
    
}