package nl.kii.template

import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CorsHandler

import static extension io.vertx.core.logging.LoggerFactory.getLogger
import static extension io.vertx.ext.web.Router.router
import static extension java.util.Objects.requireNonNull
import io.vertx.serviceproxy.ProxyHelper

class Verticle extends AbstractVerticle {
	
	val logger = class.logger
	
	override start() throws Exception {
		super.start()
		logger.info( 'Starting Verticle...' )
		logger.debug( 'Config: {}', config )
		
		val httpConfig = config.getJsonObject( 'http' ).requireNonNull( 'HTTP config must be specified' )
		val http = vertx.createHttpServer( new HttpServerOptions( httpConfig ) )

        val service = new ServiceImpl
        ProxyHelper.registerService( Service, vertx, service, 'vertx.service' )
        
        val proxy = ProxyHelper.createProxy( Service, vertx, 'vertx.service' )
		
		val extension router = vertx.router
		
		route.handler( BodyHandler.create )
		route.handler( CorsHandler.create( '*' ).allowedHeader('Authorization') )
		
		route( '/health' ).handler [ ctx |
		    proxy.healthCheck [
		        if ( succeeded ) {
		            ctx.response.setStatusCode(200).end( 'OK' )
		        } else {
                    ctx.response.setStatusCode(500).end( 'NOT OK' )
		        }
		    ]
		]
	
		http.requestHandler [ accept ]
			.listen
	}
	
}