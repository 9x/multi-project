# Multi Java/Xtend project with Gradle

Project structure:

```
/root
| - /api
|   | - api.gradle
| - /core
|   | - core.gradle
| - /web
|   | - web.gradle
| - build.gradle
| - settings.gradle
| - gradle.properties
```


#### build.gradle

* Java/Xtend nature for sub-projects
* Maven for release & snapshot
* Eclipse

#### api.gradle

* Java Annotation Processing ( command line only ) [aka Xtend Active Annotations] 

#### web.gradle

* Docker Build
* Fat Jar
* AWS Elastic Beanstalk ( Zip with procfile & cloudwatch config )
* Zip/Tar Distribution ( with shell script )
* Java application executor