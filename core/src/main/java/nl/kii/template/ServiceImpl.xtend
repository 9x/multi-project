package nl.kii.template

import io.vertx.core.Handler
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import static extension io.vertx.core.logging.LoggerFactory.getLogger
import java.util.Random

class ServiceImpl implements Service {
    
    val logger = class.logger
    
    override healthCheck(Handler<AsyncResult<Void>> result) {
        logger.info( 'Processing health check...' )
        val random = new Random
        if ( random.nextBoolean ) {
            result.handle( Future.succeededFuture )
        } else {
            result.handle( Future.failedFuture( 'Something went wrong' ) )
            logger.warn( 'Health check fails' )
        }
    }
    
}